FROM netboxcommunity/netbox:v4.2.5@sha256:80214cb37fc8ae48a5d9b01b44c350b0cf602a64460019df441a0cf073a1cca1
COPY ./plugin_requirements.txt /opt/netbox/
RUN export DEBIAN_FRONTEND=noninteractive \
    && apt-get update -qq \
    && apt-get install \
      --yes -qq --no-install-recommends \
      git
RUN /usr/local/bin/uv pip install -r /opt/netbox/plugin_requirements.txt
#COPY config/configuration.py /etc/netbox/config/configuration.py
COPY config/plugins.py /etc/netbox/config/plugins.py
RUN SECRET_KEY="dummydummydummydummydummydummydummydummydummydummy" /opt/netbox/venv/bin/python /opt/netbox/netbox/manage.py collectstatic --no-input
