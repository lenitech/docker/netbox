# Netbox

Docker image for running [Netbox](https://github.com/netbox-community/netbox) in Docker (eg. in Kubernetes).

Based on [netboxcommunity/netbox](https://github.com/netbox-community/netbox-docker) with additionals plugins.

## Plugins

- [netbox-floorplan](https://github.com/netbox-community/netbox-floorplan-plugin)
- [netbox-topology-views](https://github.com/netbox-community/netbox-topology-views)
